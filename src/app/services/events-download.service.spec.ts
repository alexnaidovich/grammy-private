import { TestBed, inject } from '@angular/core/testing';

import { EventsDownloadService } from './events-download.service';

describe('EventsDownloadService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [EventsDownloadService]
    });
  });

  it('should be created', inject([EventsDownloadService], (service: EventsDownloadService) => {
    expect(service).toBeTruthy();
  }));
});
