import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-greet',
  templateUrl: './greet.component.html',
  styleUrls: ['./greet.component.css']
})
export class GreetComponent implements OnInit {

  constructor() { }

  audio: any;
  audioIsActive: boolean;
  equializerElements: any;
  pathToTheSong: string;

  ngOnInit() {
    this.pathToTheSong = '../../../assets/audio/Артем Пивоваров – Зависимы.mp';
    this.audio = new Audio(this.pathToTheSong);
    this.audio.load();
    this.audio.play();
    this.audio.loop = true;
    this.audioIsActive = true;

    this.equializerElements = document.querySelectorAll('#equalizerBtn span');
  }

  toggleAudioState() {
    this.audioIsActive ? this.audio.pause() : this.audio.play();
    this.audioIsActive = this.audioIsActive ? false : true;

    this.equializerElements.forEach(elem => {
      elem.classList.toggle('play-audio');
    });
  }

}
