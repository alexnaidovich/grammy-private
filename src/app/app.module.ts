import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { HomeComponent } from './pages/home/home.component';
import { EventsComponent } from './pages/events/events.component';
import { MenuComponent } from './pages/menu/menu.component';
import { BlogComponent } from './pages/blog/blog.component';
import { GalleryComponent } from './pages/gallery/gallery.component';
import { AboutComponent } from './pages/about/about.component';
import { ContactsComponent } from './pages/contacts/contacts.component';
import { GreetComponent } from './shared/greet/greet.component';

import { EventsDownloadService } from './services/events-download.service';

const ROUTES: Routes = [
  {
    path: '',
    component: HomeComponent
  },
  {
    path: 'about',
    component: AboutComponent
  },
  {
    path: 'blog',
    component: BlogComponent
  },
  {
    path: 'contacts',
    component: ContactsComponent
  },
  {
    path: 'events',
    component: EventsComponent
  },
  {
    path: 'gallery',
    component: GalleryComponent
  },
  {
    path: 'menu',
    component: MenuComponent
  },
];

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    EventsComponent,
    MenuComponent,
    BlogComponent,
    GalleryComponent,
    AboutComponent,
    ContactsComponent,
    GreetComponent
  ],
  imports: [
    HttpClientModule,
    BrowserModule,
    RouterModule.forRoot(ROUTES)
  ],
  providers: [EventsDownloadService],
  bootstrap: [AppComponent]
})

export class AppModule { }