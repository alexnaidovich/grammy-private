import { Component, OnInit } from '@angular/core';
import { EventsDownloadService } from '../../services/events-download.service';
import { Events } from '../../interfaces/events';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
  providers: [EventsDownloadService]
})
export class HomeComponent implements OnInit {

  constructor(private EventsDownloadService: EventsDownloadService) { }

  eventsData: Events;
  showPage: any;

  ngOnInit() {
    this.EventsDownloadService.eventsDownload('https://grammy-admin.herokuapp.com/v1/events/findByPage')
      .then( response => {
        this.eventsData = response;
      })

    this.showPage = document.querySelector('#show-page');
    setTimeout(() => {
      this.showPage.classList.remove('hidden');
    }, 350);
  }

}
