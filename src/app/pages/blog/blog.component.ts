import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-blog',
  templateUrl: './blog.component.html',
  styleUrls: ['./blog.component.css']
})
export class BlogComponent implements OnInit {

  newsData = [
    {
      description: `Давно выяснено, что при оценке дизайна и композиции читаемый текст 
      мешает сосредоточиться. Lorem Ipsum используют`
    },
    {
      description: `Давно выяснено, что при оценке дизайна и композиции читаемый текст 
      мешает сосредоточиться. Lorem Ipsum используют`
    },
    {
      description: `Давно выяснено, что при оценке дизайна и композиции читаемый текст 
      мешает сосредоточиться. Lorem Ipsum используют`
    },
    {
      description: `Давно выяснено, что при оценке дизайна и композиции читаемый текст 
      мешает сосредоточиться. Lorem Ipsum используют`
    }
  ]

  showPage: any;

  constructor() { }

  ngOnInit() {
    this.showPage = document.querySelector('#show-page');
    setTimeout(() => {
      this.showPage.classList.remove('hidden');
    }, 350);
  }

}
