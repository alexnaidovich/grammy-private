import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})

export class MenuComponent implements OnInit {

  dishData = [
    {
      img: '../../assets/images/dish1.png',
      category: 'Основное меню'
    },
    {
      img: '../../assets/images/dish2.png',
      category: 'Event меню'
    },
    {
      img: '../../assets/images/dish3.png',
      category: 'Винная карта'
    }
  ];

  showPage: any;

  constructor() { }

  ngOnInit() {
    this.showPage = document.querySelector('#show-page');
    setTimeout(() => {
      this.showPage.classList.remove('hidden');
    }, 350);
  }

}
