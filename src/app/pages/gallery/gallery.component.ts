import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-gallery',
  templateUrl: './gallery.component.html',
  styleUrls: ['./gallery.component.css']
})
export class GalleryComponent implements OnInit {

  eventsData = [
    {
      title: 'Встречайте квартал-95',
      subtitle: 'Встречайте квартал-95'
    },
    {
      title: 'Встречайте квартал-95',
      subtitle: 'Встречайте квартал-95'
    },
    {
      title: 'Встречайте квартал-95',
      subtitle: 'Встречайте квартал-95'
    },
    {
      title: 'Встречайте квартал-95',
      subtitle: 'Встречайте квартал-95'
    },
  ];

  showPage: any;

  constructor() { }

  ngOnInit() {
    this.showPage = document.querySelector('#show-page');
    setTimeout(() => {
      this.showPage.classList.remove('hidden');
    }, 350);
  }

}
