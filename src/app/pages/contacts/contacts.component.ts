import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-contacts',
  templateUrl: './contacts.component.html',
  styleUrls: ['./contacts.component.css']
})
export class ContactsComponent implements OnInit {

  showPage: any;

  constructor() { }

  ngOnInit() {
    this.showPage = document.querySelector('#show-page');
    setTimeout(() => {
      this.showPage.classList.remove('hidden');
    }, 350);
  }

}
